import java.util.Scanner;

public class Newton {
	
	public static int n;
	private double x[];
	private double y[];
	
	Scanner reader = new Scanner(System.in);
	
	public Newton(){
		System.out.print("Podaj n: ");
		n = reader.nextInt();
		x = new double[n];
		y = new double[n];
		for(int i=0;i<n;i++){
			System.out.print("Podaj x[" + i + "]: ");
			x[i] = reader.nextDouble();
			System.out.print("Podaj y[" + i + "]: ");
			y[i] = reader.nextDouble();
		}
	}
	
	void wypiszTablice(){
		System.out.printf("x  ||");
		for(int i=0; i<n; i++){
			System.out.printf(" %7.2f |",x[i]);
		}
		System.out.println();
		System.out.printf("y  ||");
		for(int i=0; i<n; i++){
			System.out.printf(" %7.2f |",y[i]);
		}
		System.out.println();
	}
	
	double b(int xmin, int xmax){		//xmin i xmax to miejsca(przedzial) w tablicy
		if(xmin==xmax) return y[xmin];
		else{
			return (b(xmin+1,xmax) - b(xmin,xmax-1))/(x[xmax] - x[xmin]);
		}
	}
	
	double[] wyznaczB(){
		double b[] = new double[n];
		for(int i=n-1;i>=0;i--){
			b[i]=b(0,i);
		}
		return b;
	}
	
	public static void main(String[] args){
		Newton zestaw = new Newton();
		zestaw.wypiszTablice();
		double b[] = zestaw.wyznaczB();
		for(int i=0; i<n; i++) System.out.println("b[" + i + "]= " + b[i]);
	}
}
