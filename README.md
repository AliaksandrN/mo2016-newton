# README #

Wyliczanie b działczy jak trzeba, sprawdzane na przykładzie z wykładu dla danych:  
| x  |   -1,00 |    0,00 |    1,00 |    2,00 |  
| y  |   -7,00 |   -5,00 |   -3,00 |    5,00 |

n = 3;
P(x) = b0 + b1(x - x0) + b2(x - x0)(x - x1) +b3(x - x0)(x - x1)(x - x2) = b0 + b1(x + 1) + b2(x + 1)x +b3(x + 1)x(x - 1);
-7 => P(-1) = -7;
-5 => P(0) = -7 + b1(0 + 1) + 0 + 0; b1 - 7 = -5; b1 = 2;
-3 => P(1) = -7 + 2(1 + 1) + b2(1 + 1)(1 - 0) + 0; 2b2 + 4 - 7 = -3; b2 = 0;
 5 => P(2) = -7 + 2(2 + 1) + 0(2 + 1)(2 - 0) + b3(2 + 1)(2 - 0)(2 - 1); -7 + 6 + 6b3 = 5; b3 = 1;
b0 = -7;
b1 =  2;
b2 =  0;
b3 =  1;
P(x) = -7 + 2(x + 1) + 1(x + 1)x +0(x + 1)x(x - 1) = x^2 +3x - 5;

Pierwotna P(x) to 1/3x^3 + 3/2x^2 - 5x

i

| x  |    1,00 |    2,00 |    4,00 |  
| y  |    0,00 |    2,00 |   12,00 |


TODO:  
* Podstawienie b do wzoru i wyliczenie/uproszczenie go  
* Całka  
* Dokumentacja


Na 25.10:  
* Opis algorytmu  
* Przykład rozwišzany ręcznie